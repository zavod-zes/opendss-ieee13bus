#import numpy as np
import numpy as np

from custompythonlibs.OpenDSSn import OpenDSSBasic as ODB

from custompythonlibs.Yde import Y_dssMatrix as Ydsm
# %% Generiranje Y matrice iz OpenDSS u Python
'''
#dss_script = r'E:\OneDrive\OneDrive - etfos.hr\ZNANSTVENO-PhD\ZNANSTVENO\POSTDOKTORSKI\ZNANST-RADOVI\2019\HRZZ-Eurocon2019-ANNpf\PYTHON\IEEE13NodecktPY.dss' # kuca
dss_script = r'E:\OneDrive - etfos.hr\ZNANSTVENO-PhD\ZNANSTVENO\POSTDOKTORSKI\ZNANST-RADOVI\2019\HRZZ-Eurocon2019-ANNpf\PYTHON\IEEE13NodecktPY.dss' # poso
dssObject = ODB(dss_script)
print (dssObject)

# print(dssObject.get_y_matrix("WITH_LOAD"))
# print(dssObject.get_y_matrix("NO_LOAD"))
# print(dssObject.get_y_matrix("POS_SEQ_WITH_LOAD"))
# print(dssObject.get_y_matrix("POS_SEQ_NO_LOAD"))
dssObject.get_dss_obj()

Yl = dssObject.get_y_matrix('WITH_LOAD')
Ynl = dssObject.get_y_matrix('NO_LOAD')
Ylb = dssObject.get_y_matrix('POS_SEQ_WITH_LOAD')
Ynlb = dssObject.get_y_matrix('POS_SEQ_NO_LOAD')
Ynlg = dssObject.get_y_matrix('NO_LaG')
Ynlgb = dssObject.get_y_matrix('POS_SEQ_NO_LaG')
'''

# %% Zapis Y matrice u formi [n x n] numpy matrice
#Ym = Ynlg # Yl Ynl Ylb Ynlb Ynlg Ynlgb
#dss_script = r'E:\OneDrive\OneDrive - etfos.hr\ZNANSTVENO-PhD\ZNANSTVENO\POSTDOKTORSKI\ZNANST-RADOVI\2019\HRZZ-Eurocon2019-ANNpf\PYTHON\IEEE13NodecktPY.dss' # kuca
dss_script = r'E:\OneDrive - etfos.hr\ZNANSTVENO-PhD\ZNANSTVENO\POSTDOKTORSKI\ZNANST-RADOVI\2019\HRZZ-Eurocon2019-ANNpf\PYTHON\IEEE13NodecktPY.dss' # poso
dssObject = ODB(dss_script)

flagx = ODB.get_flags()

obj = ['Y samo direktnog sustava bez trosila',
       'Y samo dirketnog sustava s trosilima',
       'Puna (jednofazna) Y bez trosila',
       'Puna (jednofazna) Y s trosilima',
       'Puna (jednofazna) Y bez trosila i s iskljucenim Vsource',
       'Y samo direktnog sustava bez trosila i s iskljucenim Vsource']
fl = 2
print ('\nSpektralna dekompozicija Y matrice sustava za slucaj:\n' + Fore.RED  + obj[fl])


Ym = dssObject.get_y_matrix(flagx[fl])

BUSnames, NODEnames = dssObject.get_dss_busN()
if fl == 0 or fl == 1 or fl == 5:
    fz = 1
else:
    fz = 3
BUSnames = BUSnames[fz:]

Yobj = Ydsm(Ym)

Ymx = Yobj.Y_make()

Dekomp, Lam, Svect, Reciplam = Yobj.Y_spectr(fz)
LamA = abs(Lam)
# indeksi uzlazno sortiranih svoj.vrijednosti
LamMinSort = np.argsort(LamA)
print ('\nNajmanje svojstvene vrijednosti u dijagonali Lambda matrice su na pozicijama:\n', LamMinSort)
minL = np.argmin(LamA)


Wdes, Wlij, osj1, osj2 = Yobj.Y_sens(minL, fz)

os1A = abs(osj1)
os2A = abs(osj2)

osDIAG = np.diag(os1A)

# Sabirnice sortiraen uzlazno po osjetljivosti, najosjetljivije su na kraju liste
sortBusOs = sorted(zip(osDIAG, BUSnames))

print ('\nZa promatranu svojstvenu vrijednost najosjetljviji je cvor:\n', sortBusOs[-1])
